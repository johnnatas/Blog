<?php $this->load->view('includes/header'); ?>
<div class="container">
    <div ng-app="cadastro">
        <div id="form" class="col-md-offset-3" ng-controller="cadastroCtrl">
            <h1>Cadastre-se aqui</h1>            
                <form method="post" action="/user/registrar" class="col-md-8" name="cadastroform">                    
                    <input class="form-control" type="text" name="nome" placeholder="Nome" ng-model="cadastrado.nome" ng-required="true">
                    <input class="form-control" type="text" name="sobrenome" placeholder="Sobrenome" ng-model="cadastrado.sobrenome" ng-required="true">                    
                    <input class="form-control" type="email" name="email" placeholder="E-mail" ng-model="cadastrado.email" ng-required="true">
                    <input class="form-control" type="password" name="senha" placeholder="Senha" ng-required="true">
                    <input class="form-control" type="password" name="repetirsenha" placeholder="Repetir senha" ng-model="cadastrado.repetirsenha" ng-required="true">
                    <button type="submit" class="btn btn-success btn-block" ng-disabled="cadastroform.$invalid">Cadastrar</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>