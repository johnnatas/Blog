<?php $this->load->view('includes/header') ?>

       <div class="container" ng-app="login"> 
       
            <div id="form" class="col-md-offset-4" ng-controller="loginCtrl">
            <h1>Logar</h1>
                <form method="post" action="/user/logar" class="col-md-5">                  
                    <input class="form-control" type="email" name="email" placeholder="E-mail" ng-model="usuario.email">
                    <input class="form-control" type="password" name="senha" placeholder="Senha" ng-model="usuario.senha">
                    <a href="<?php echo base_url('/recuperar_senha') ?>">Esqueci minha senha</a>
                    <button class="btn btn-success btn-block">Acessar</button>
                </form>
                <div id="message">
                    <?php  ?>
                </div>
            </div>
        </div>

<?php $this->load->view('includes/footer') ?>