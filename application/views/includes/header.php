<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?php echo $titulo ?></title>
        <meta charset="UTF-8">
        <link href="<?php echo base_url('node_modules/bootstrap/dist/css/bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/style.css') ?>" rel="stylesheet">
    </head>
    <body>
       <header id="header">   
        <div class="container" >
            <div class="row">
                <a href="<?php echo base_url('/') ?>">
                    <img src="<?php echo base_url('assets/img/arrayenterprises.png') ?>" class="img-responsive col-md-2">
                </a>
                <nav class="nav navbar">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
                            <span class="sr-only">Menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="menu" class="col-md-offset-9">
                        <ul>
                            <?php 
                                if($this->session->has_userdata('logado')):
                                    echo '
                                    <li class="nav-link"><a class="col-md-12" href="' . base_url('/') . '">Inicio</a></li>
                                    <li class="nav-link"><a class="col-md-12" href="' . base_url('cadastrar') . '">Perfil</a></li>
                                    <li class="nav-link"><a class="col-md-12" href="' . base_url('user/logout') . '">Logout</a></li>
                                    ';
                                else:
                                    echo '
                                    <li class="nav-link"><a class="col-md-12" href="' . base_url('/') . '">Inicio</a></li>
                                    <li class="nav-link"><a class="col-md-12" href="' . base_url('cadastrar') . '">Cadastre-se</a></li>
                                    <li class="nav-link"><a class="col-md-12" href="' . base_url('logar') . '">Login</a></li>
                                    ';
                                endif;
                            ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
       </header>