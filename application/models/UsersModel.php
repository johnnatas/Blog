<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model {

	function __constuct(){
		parent::__constuct();
	}

	public function inserir($dados = NULL)
	{
		if($dados != null):
			$this->db->insert('users', $dados);
		endif;
    }
    
    public function listar()
	{
		$query = $this->db->get('users');
		return $query->result();
    }
    
    public function alterar()
	{
		
    }
    
    public function excluir()
	{
		
	}

	public function verificaLogin($dados){
		if($dados != null):
			//dados a serem verificados armazenados em variáveis
			$email = $dados['email'];
			$senha = $dados['senha'];

			//confirmando a existência do email digitado
			$confEmail = $this->verificaEmail($email);
			
			//Se existe verifica se a senha digitada é igual a armazenada no banco
			if($confEmail):
				$busca = $this->verificaSenha($email);
				
				//verifica se as senhas são iguais
				if($busca != $senha):
					return false;
				else:
					return true;
				endif;
				
			else:
				return false;
			endif;
		endif;
	}

	public function verificaEmail($email){
		
		//confirmando a existência do email digitado
		$this->db->select('email')->where('email', $email);
		$confEmail = $this->db->get('users')->num_rows();

		//se o e-mail já existir retorna true, senão, retorna false e o cadastro prossegue
		if($confEmail > 0):
			return true;
		else:
			return false;
		endif;
	}

	public function verificaSenha($email){
		//consulta a senha registrada
		$this->db->select('senha')->where('email', $email)->limit(1);
		$query = $this->db->get('users');

		foreach ($query->result() as $row):
			$senhaUsuario = $row->senha;
		endforeach;
		
		//descriptografa a senha
		$decodePass = $this->decryptpass($senhaUsuario);
		return $decodePass;
	}

	public function abreSessao($email){

		$query = $this->db->select('nome, email')->where('email', $email)->get('users');

		foreach($query->result() as $row):
			$nome = $row->nome;
			$email = $row->email;
		endforeach;

			$userdata = array('nome' => $nome, 'email' => $email, 'logado' => true);
			$this->session->set_userdata($userdata);

	}


	protected function decryptpass($palavra){
        //descriptografando a senha
        $this->load->library('encryption');
        $decryptpwd = $this->encryption->decrypt($palavra);
        return $decryptpwd;
	}

}
