<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller {

	function __constuct(){
		parent::__constuct();
	}

	//Pagina Home
	public function index()
	{
		//Título da página
		$dados['titulo'] = "Início - Array Enterprise";

		//carregando model
		$this->load->model("usersModel", "users");

		//armazenando o retorno do método num array
		$dados['users'] = $this->users->listar();

		//carregando a view e passando o array de dados
		$this->load->view('home', $dados);
	}

	//Pagina de Cadastro
	public function cadastrar()
	{
		$dados['titulo'] = "Cadastre-se - Array Enterprise";
		$this->load->view('cadastrar', $dados);
	}

	//Página de Login
	public function logar()
	{
		$dados['titulo'] = "Login - Array Enterprise";
		$this->load->view('logar', $dados);
	}

	//Página perfil do usuário
	public function perfil(){
		$dados['titulo'] = "Perfil - Array Enterprise";
		$this->load->view('perfil', $dados);
	}

	//Página de recuperação de senha
	public function recuperar_senha(){
		$dados['titulo'] = "Recuperar Senha - Array Enterprise";
		$this->load->view('recuperar_senha', $dados);
	}
}