<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __constuct(){
		parent::__constuct();		
	}
    
    //registrar no DB
	public function registrar(){

        $this->load->model("usersModel", "users");
        
        // armazenando $_POST['campo'] em variáveis para melhor visualização
        $nome = $this->input->post('nome'); 
        $sobrenome = $this->input->post('sobrenome');
        $senha = $this->input->post('repetirsenha');

        //gravando um array $dados com as informações do form passadas por post
        $dados['nome'] = $nome . " " .$sobrenome;
        $dados['email'] = $this->input->post('email');        
        $dados['senha'] = $this->encryptpass($senha);

        //chama método de verificação de existência de e-mail
        $existeEmail = $this->users->verificaEmail($dados['email']);

        if($existeEmail):
            echo 'Este e-mail já está cadastrado';
        else:
            //chamando método de registrar usuario passando o array $dados como parâmetro
            $this->users->inserir($dados);

            redirect('/');
        endif;

    }

    public function logar(){

        $this->load->model("usersModel", "user");

        $dados['email'] = $this->input->post('email');
        $dados['senha'] = $this->input->post('senha');

        $validacao = $this->user->verificaLogin($dados);
        if($validacao):
            $this->user->abreSessao($dados['email']);
            redirect('/');
        else:   
            $dados['msg'] = "Vai dá não";
            $this->load->view('logar', $dados);
        endif;
    }


    public function logout(){
		$userdata = array('nome', 'email', 'logado');
        $this->session->unset_userdata($userdata);
        redirect('/');
    }
    
    public function recuperar_senha(){
        $this->load->model('usersModel', 'user');

        $email = $this->input->post('email');
        $confEmail = $this->user->verificaEmail($email);

        if($confEmail):
            $senha = $this->user->verificaSenha($email);
            $this->load->library('email');

            //enviando e-mail
            $this->email->from('suporte@colegionovomundo.com.br', 'Jonatas')
            ->message('Sua senha é: ' . $senha)
            ->send();

    }

    //Método de criptografia para registro de senha
    protected function encryptpass($palavra){
        //criptografando a senha
        $this->load->library('encryption');
        $encryptpwd = $this->encryption->encrypt($palavra);
        return $encryptpwd;
    }

}
